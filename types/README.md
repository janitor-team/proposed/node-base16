# Installation
> `npm install --save @types/base16`

# Summary
This package contains type definitions for base16-js (https://github.com/gaearon/base16-js).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/base16.

### Additional Details
 * Last updated: Sat, 08 Aug 2020 02:10:40 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Alec Hill](https://github.com/alechill), and [Nathan Bierema](https://github.com/Methuselah96).
